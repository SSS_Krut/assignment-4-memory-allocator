#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#define BLOCK_MIN_CAPACITY 24

void debug_block(struct block_header *b, const char *fmt, ...);
void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);
extern inline block_capacity capacity_from_size(block_size sz);

inline static size_t pages_count(size_t mem) {
  return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
inline static size_t round_pages(size_t mem) {
  return getpagesize() * pages_count(mem);
}

static void block_init(void *restrict addr, block_size block_sz,
                       void *restrict next) {
  *((struct block_header *)addr) = (struct block_header){
      .next = next, .capacity = capacity_from_size(block_sz), .is_free = true};
}

static size_t region_actual_size(size_t query) {
  return max(round_pages(query), REGION_MIN_SIZE);
}

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags) {
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE,
              MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region(void const *addr, size_t query) { //+
  block_capacity requested_capacity = {.bytes = query};
  block_size requested_size = size_from_capacity(requested_capacity);
  size_t real_size = region_actual_size(requested_size.bytes);

  struct region mapped_pages;
  mapped_pages.addr = map_pages(addr, real_size, MAP_FIXED_NOREPLACE);
  if (mapped_pages.addr == MAP_FAILED)
    mapped_pages.addr = map_pages(addr, real_size, 0);
  if (mapped_pages.addr == MAP_FAILED)
    return REGION_INVALID;

  requested_size.bytes = mapped_pages.size = real_size;
  mapped_pages.extends = mapped_pages.addr == addr;

  block_init(mapped_pages.addr, requested_size, NULL);

  return mapped_pages;
}

void *heap_init(size_t initial) { //+-
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

static void *
block_after(struct block_header const *block); // return addr of the next block
static bool
blocks_continuous(struct block_header const *fst,
                  struct block_header const *snd); // return is blocks are near

/*  освободить всю память, выделенную под кучу */
void heap_term() { //+
  struct block_header *start = HEAP_START;
  struct block_header *current = start;
  struct block_header *next;

  for (size_t bytes_row = 0; current != NULL; current = next) {
    next = current->next;
    bytes_row += size_from_capacity(current->capacity).bytes;

    if (!blocks_continuous(current, next)) {
      munmap(start, bytes_row);
      start = next;
      bytes_row = 0;
    }
  }
}

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable(struct block_header *restrict block,
                             size_t query) {
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <=
             block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) { //+-
  if (!block || !block_splittable(block, query))
    return false;

  query = max(query, BLOCK_MIN_CAPACITY);

  block_capacity current_capacity = {.bytes = query};
  block_size current_size = size_from_capacity(current_capacity);

  block_capacity next_capacity = {.bytes = block->capacity.bytes -
                                           current_size.bytes};
  block_size next_size = size_from_capacity(next_capacity);

  void *next_addr = (void *)block + current_size.bytes;
  block_init(next_addr, next_size, block->next);
  block->capacity = current_capacity;
  block->next = (struct block_header *)next_addr;

  return true;
}

static bool block_is_big_enough(size_t query, struct block_header *block) {
  return block->capacity.bytes >= query;
}

/*  --- Слияние соседних свободных блоков --- */

inline static void *block_after(struct block_header const *block) {
  return (void *)(block->contents + block->capacity.bytes);
}
inline static bool blocks_continuous(struct block_header const *fst,
                                     struct block_header const *snd) {
  return (void *)snd == block_after(fst);
}

inline static bool mergeable(struct block_header const *restrict fst,
                             struct block_header const *restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *current_block) { //+
  if (!current_block)
    return false;

  struct block_header *next_block = current_block->next;
  if (!next_block || !mergeable(current_block, next_block))
    return false;

  current_block->next = next_block->next;
  current_block->capacity.bytes +=
      size_from_capacity(next_block->capacity).bytes;

  return true;
}

static void merge_all_next_free(struct block_header *block) {
  for (; try_merge_with_next(block);)
    ;
}

/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header *block;
};

static struct block_search_result
find_good_or_last(struct block_header *restrict block, size_t sz) { //+-
  struct block_search_result result = {.block = block, .type = BSR_CORRUPTED};
  if (!block)
    return result;

  for (; result.block != NULL; result.block = result.block->next) {
    merge_all_next_free(result.block);
    if (result.block->is_free && block_is_big_enough(sz, result.block)) {
      result.type = BSR_FOUND_GOOD_BLOCK;

      return result;
    }

    if (!result.block->next) {
      result.type = BSR_REACHED_END_NOT_FOUND;
      return result;
    }
  }

  result.type = BSR_REACHED_END_NOT_FOUND;
  return result;
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь
 расширить кучу Можно переиспользовать как только кучу расширили. */
static struct block_search_result
try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result search_result = find_good_or_last(block, query);

  if (search_result.type == BSR_CORRUPTED ||
      search_result.type == BSR_REACHED_END_NOT_FOUND) {
    return search_result;
  }

  split_if_too_big(search_result.block, query);
  search_result.block->is_free = false;

  merge_all_next_free(search_result.block->next);

  return search_result;
}

static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query) {
  if (last == NULL)
    return NULL;

  void *block_after_ptr = block_after(last);
  struct region new_region = alloc_region(block_after_ptr, query);
  if (region_is_invalid(&new_region))
    return NULL;

  last->next = new_region.addr;
  if (new_region.extends) {
    merge_all_next_free(last);
    if (last->is_free)
      return last;
  }
  return last->next;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header *memalloc(size_t query,
                                     struct block_header *heap_start) {
  if (heap_start == NULL)
    return NULL;

  size_t real_query = max(query, BLOCK_MIN_CAPACITY);

  struct block_search_result search_result =
      try_memalloc_existing(real_query, heap_start);

  switch (search_result.type) {
  case BSR_FOUND_GOOD_BLOCK:
    return search_result.block;

  case BSR_REACHED_END_NOT_FOUND:
    break;

  case BSR_CORRUPTED:
    debug("Critical Error");
    return NULL;

  default:
    return NULL;
  }

  struct block_header *new_block = grow_heap(search_result.block, real_query);
  if (new_block == NULL)
    return NULL;

  search_result = try_memalloc_existing(real_query, new_block);
  return search_result.block;
}

void *_malloc(size_t query) {
  size_t size = max(query, BLOCK_MIN_CAPACITY);
  size_t size_with_align = size + BLOCK_ALIGN - size % BLOCK_ALIGN;
  struct block_header *const addr =
      memalloc(size_with_align, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  else
    return NULL;
}

static struct block_header *block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) -
                                 offsetof(struct block_header, contents));
}

void _free(void *mem) {
  if (!mem)
    return;
  struct block_header *header = block_get_header(mem);
  header->is_free = true;
  merge_all_next_free(header);
}
